Fork of https://hub.docker.com/r/jrei/systemd-ubuntu and enhanced w/ arm64.

# Docker images for running systemd

You can use this images as base containers to run systemd services inside.

## Usage

1. Run the container as a daemon

`docker run -d --name systemd --tmpfs /tmp --tmpfs /run --tmpfs /run/lock -v /sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/morph027/systemd-ubuntu:$VERSION`

or if it doesn't work

`docker run -d --name systemd --privileged -v /sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/morph027/systemd-ubuntu:$VERSION`

2. Enter to the container

`docker exec -it systemd bash`

3. Remove the container

`docker rm -f systemd`

## Enhanced usage

### Run arm64 container from amd64 host

1. Install qemu for arm

`sudo apt install qemu-user-static`

2. Run the container as daemon

`docker run -d --name systemd --tmpfs /tmp --tmpfs /run --tmpfs /run/lock -v /sys/fs/cgroup:/sys/fs/cgroup:ro -v /usr/bin/qemu-aarch64-static:/usr/bin/qemu-aarch64-static registry.gitlab.com/morph027/systemd-ubuntu:$VERSION-arm64`
